# README #

This README document describes the BARCAST code.

* Original code: M.P. Tingley, JClim (2010) in MatLab
* This code: JP Werner, Clim.Past (2015) in R + Fortran
* The code has also been used in Luterbacher et al., ERL (2016)
* The code is undergoing several extensions and some rewrites.

## What is this repository for? ##

* BARCAST is the Bayesian Algorithm to Reconstruct Climate Anomalies in Space and Time

## Setup ##

This describes the process from the command line.
    
    # Checkout the Barcast code from this git repository
    git clone https://jopewerner@bitbucket.org/jopewerner/barcast.git
    # create a build directory under a new folder. Most convenient is (IMHO) a
    # subfolder under the same parent as the Barcast git.
    mkdir build_barcast
    # change into the build folder
    cd build_barcast
    # configure 
    ccmake ../barcast 
    # look at the output. A "missing find OpenBLAS" warning can be ignored (unless you 
    # want to use OpenBLAS).
    # Finally, build the project
    make
    make install
    # change into the installation directory
    cd ~/BB_barcast/bin
    # Make the test run execuatable and run the test
    chmod 755 RunBarcastTest.sh
    ./RunBarcastTest.sh


## Required software and libraries ##

* Fortran compiler (such as gfortran)
* Fortran libraries (Linux: the `-dev` packages)
    * BLAS
    * LAPACK
    * or OpenBLAS (can replace BLAS, sometimes also LAPACK)
* Openmpi
* cmake / ccmake
* R and the following R libraries:
    * Rmpi
    * coda
    * ggmcmc
    * foreach
    * spam

    as well as the respective dependencies

## Current / planned changes ##

### Changes I am working on ###

* Installation scripts, as flexible as possible (different systems, installations)
* Move the covariance matrix calculations to Fortran (in sparse matrix representation)
* include code to do calculations split over several jobs: The cluster admins usually do not like it if your jobs run more than a few days...
* Re-enable the code to run without MPI (Rmpi) if no parallel tempering is needed.

### Changes that are planned for the near(-ish) future ###

* Extend to low resolution code
* Write a module to use indexed data (Pfister-indices)

### Contribution guidelines ###

If you want to contribute to the code, please make sure to

* use a similar variable naming scheme
* do not brake the variable dimensions - or fix across the whole code
* try to stay as backwards compatible as possible
* use test data as created with the `ConstructTestDatavNM.R` and `create_t-uncertain_test.R` scripts
 to show the changes
    * do not affect validity of the old versions
    * work for the newly included data / process / variable types.
    * and please include the new test data creation scripts / typical test data in your changes
* Create a pull request for me (Joe Werner) to review the code changes and check for compatibility
* Be sure to notify me of
    * use in articles (citing me)
    * changes of code in articles (me citing you)


### Who do I talk to? ###

* Drop me an email / message, either through the BitBucket interface or
  `johannes.werner@uib.no` or even `johannes.werner@physik.tu-darmstadt.de`