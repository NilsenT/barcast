# The below is for using OpenBLAS only. There is a method to use an R package
# (https://hyperspec.wordpress.com/2012/07/26/altering-openblas-threads/), but
# this absolutely requires to have OpenBLAS installed - and I do not want to
# write yet another check. So: This is done / set by the user 
# Remark: running 3 chains on 12 cores means you could set the THREADS to 4.
# Depending on the actual size of the matrices the benefit might be neglegible
# (r it might be detrimental, even!).

export OPENBLAS_NUM_THREADS=1

# run BARCAST for the first time
echo "source(\"BARCASTMainCode_Rmpi.R\");
Run.Barcast(\"../TestData\",\"Certain+Uncertain_Data.RData\",nChains=3, Sampler.Its=200, pre.Sampler.Its=200, swap.interval=20, CurrCont=F)" | R --vanilla

# Extend the run
echo "source(\"BARCASTMainCode_Rmpi.R\");
Run.Barcast(\"../TestData\",\"Certain+Uncertain_Data.RData\",nChains=3, Sampler.Its=200, pre.Sampler.Its=200, swap.interval=20, CurrCont=T)" | R --vanilla

