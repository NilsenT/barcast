# 
# Author: Martin Tingley, Joe Werner (R adaption)
###############################################################################

library("MASS")
library("stats")

Initial.vals <- function(BARCAST.datdir, BARCAST.infile, Pars.Selector = 0){
  ## SETS the initial paramter values for the MCMC procedure, as draws from
  #(at times) truncated priors. Saves the intiall values in the strcuture INITIAL_VALS 
  #IF the priors are changed in Prior_pars_vNewModel, then this should be run
  #again. 
  # Pars.Selector = 
  # set to 0 to set initial values of the parameters to the modes of priors
  # set to 1 for draws from (truncated) prior
      
  ## Scalar parameters
  load(paste(BARCAST.datdir,"/PRIORSvNewMeth1.R", sep="") )

  #figure out the number of proxy types:
  num.P <- length( grep("PROXY",names(PRIORS)))

  ##
  INITIAL.VALS <- list();
    
  if ( Pars.Selector==0 ) {
    #Initial Value of alpha: (mean):
    INITIAL.VALS$alpha<-PRIORS$alpha[1];
    #Initial Value for mu: mode of the normal prior.
    INITIAL.VALS$mu<-PRIORS$mu[1];
    #Initial Value for sigma2: mode of inverse gamma prior:
    INITIAL.VALS$sigma2<-PRIORS$sigma2[2]/(PRIORS$sigma2[1]+1);
    #Initial Value for phi: mode of the log-normal distrubution:
    INITIAL.VALS$phi<-exp(PRIORS$phi[1]-PRIORS$phi[2]);
    # Initial Value for lambda: weight of the first significant climate pattern
    INITIAL.VALS$lambda <- PRIORS$lambda[1]
    #Initial Value for tau2_I: : mode of inverse gamma prior:
    INITIAL.VALS$tau2.I=PRIORS$tau2.I[2]/(PRIORS$tau2.I[1]+1);
    # reserve space for the inverse temperature
    INITIAL.VALS$invChainTemp <- 1

    print( INITIAL.VALS)
    print(" ---- ")


    for ( kk in seq(1,num.P) ){
        betanum <- 2
        INITIAL.VALS[[kk+7]] <- list()
        if (PRIORS[[kk+6]]$type == 1) {betanum <- 3}

        #Initial Value for tau2_P: : mode of inverse gamma prior:
        INITIAL.VALS[[kk+7]]$type<-PRIORS[[kk+6]]$type
        pname<-paste("type.",toString(kk),sep="");
        names(INITIAL.VALS[[kk+7]])[1] <- pname
        pname <- paste("tau2.P.",toString(kk),sep="");
        INITIAL.VALS[[kk+7]][[2]]<-PRIORS[[kk+6]][[2]][2]/(PRIORS[[kk+6]][[2]][1]+1);
        names(INITIAL.VALS[[kk+7]])[2]<-pname;

        for (thisbeta in seq(0,betanum-1)){
          #Initial value for Beta_1: mode of the normal prior:
          pname <- paste( "Beta", thisbeta, kk, sep=".");
          INITIAL.VALS[[kk+7]][[3+thisbeta]] <- PRIORS[[kk+6]][[3+thisbeta]][1];
          pripos <- which(names(PRIORS)==pname);
          names(INITIAL.VALS[[kk+7]])[3+thisbeta]<-pname;
        }
        if (PRIORS[[kk+6]]$type == 0) {
          # set the beta_2 parameter to zero for proxies not
          # including a delayed response
          pname <- paste( "Beta", 2, kk, sep=".");
          INITIAL.VALS[[kk+7]][[5]] <- 0;
          pripos <- which(names(PRIORS)==pname);
          names(INITIAL.VALS[[kk+7]])[5]<-pname;
        }
        if (PRIORS[[kk+6]]$type == 20) {
          INITIAL.VALS[[kk+7]][[5]] <- 1
          pname <- paste( "ADM", kk, sep=".");
          names(INITIAL.VALS[[kk+7]])[5]<-pname;
        }
    }
  } else if (Pars.Selector==1){
    #Initial Value of alpha: Draw from the truncated normal:
    #TRUCNATE A Bit to set initial value near 0.5:
    INITIAL.VALS$alpha <- max( min(rnorm(1,PRIORS$alpha[1],sqrt(PRIORS$alpha[2])),1),0);

    #Initial Value for mu: draw from the normal prior.
    INITIAL.VALS$mu<-rnorm(1,PRIORS$mu[1],sqrt(PRIORS$mu[2]));

    #Initial Value for sigma2: draw from inverse gamma prior is likely a bad
    #ideas, due to the very large possible values. So truncate to below some
    #value:
    cutt<-5;
    dummy<-0;
    while (dummy==0){ # m=loc s=scale ~ a=shape
      p1<-1/rgamma(1,scale=1/PRIORS$sigma2[2],shape=PRIORS$sigma2[1]);
      if (p1<cutt){
        INITIAL.VALS$sigma2=p1;
        dummy<-1;
      }
    }

    #Initial Value for phi: draw from the log-normal distrubution, truncated to less
    #than a cutoff, determined by the prior parameters:
    cutt<-exp(PRIORS$phi[1]+2*sqrt(PRIORS$phi[2]));
    dummy<-0;
    while (dummy==0){
      p1<-rlnorm(1,PRIORS$phi[1], sqrt(PRIORS$phi[2]));
      if (p1<cutt){
        INITIAL.VALS$phi<-p1;
        dummy=1;
      }
    }
    INITIAL.VALS$lambda <- 0

    #Initial Value for tau2_I: : draw from inverse gamma prior truncated to
    #less than some cut off value:
    cutt<-5;
    dummy<-0;
    while (dummy==0){ # m=loc s=scale ~ a=shape
      p1<-1/(rgamma(1,scale=1/PRIORS$sigma2[2],shape=PRIORS$sigma2[1])+1);
      if (p1<cutt){
        INITIAL.VALS$tau2.I=p1;
        dummy<-1;
      }
    }
    # reserve space for the inverse temperature
    INITIAL.VALS$invChainTemp <- 1
    print( INITIAL.VALS)
    print(" ---- ")

    for ( kk in seq(1,num.P) ){
      betanum <- 2
      INITIAL.VALS[[kk+7]] <- list()
      if (PRIORS[[kk+6]]$type == 1) {betanum <- 3}

      INITIAL.VALS[[kk+7]]$type <- PRIORS[[kk+6]]$type
      pname<-paste("type.",toString(kk),sep="");
      names(INITIAL.VALS[[kk+7]])[1] <- pname

      pname<-paste("tau2.P.",toString(kk),sep="");
      dummy=0
      while (dummy==0){ # m=loc s=scale ~ a=shape
        p1<-1/(rgamma(1,scale=1/PRIORS[[kk+6]][[2]][2],shape=PRIORS[[kk+6]][[2]][1])+1);
        if (p1<cutt){
          INITIAL.VALS[[kk+7]][[2]] <- p1;
          dummy<-1;
        }
      }
      names(INITIAL.VALS[[kk+7]])[2]<-pname;
      print(INITIAL.VALS[[kk+7]])

      for (thisbeta in seq(0,betanum-1)){
        pname <- paste( "Beta", thisbeta, kk, sep=".");
        print(pname)
        INITIAL.VALS[[kk+7]][[3+thisbeta]] <- rnorm(1,PRIORS[[kk+6]][[3+thisbeta]][1], 
                                        PRIORS[[kk+6]][[3+thisbeta]][2] );
        print(pname)
        names(INITIAL.VALS[[kk+7]])[3+thisbeta]<-pname;
      }
      if (PRIORS[[kk+6]]$type == 0) {
        # set the beta_2 parameter to zero for proxies not
        # including a delayed response
        pname <- paste( "Beta", 2, kk, sep=".");
        INITIAL.VALS[[kk+7]][[5]] <- 0;
        names(INITIAL.VALS[[kk+7]])[5]<-pname;
      }
      if (PRIORS[[kk+6]]$type == 20) {
        INITIAL.VALS[[kk+7]][[5]] <- 1
        pname <- paste( "ADM", kk, sep=".");
        names(INITIAL.VALS[[kk+7]])[5]<-pname;
      }
    }
  }

  ## Setting the initial values of the temperature matrix. 
  load(BARCAST.infile )

  Temp.Selector=1;
  #set to 0 to set the initial value of the T(k) vector at each year to the
  #mode of the prior for T(0).
  #set to 1 to set the initial value of the T(k) vector at each year to a
  #draw from the prior for T(0).
      
  #RECALL that the temp matrix has one more column (year) than the data
  #matrices
  if (PRIORS$T.0[1] <= 0) {PRIORS$T.0[1] <- .1}
  if (Temp.Selector == 0){
    #Method 3: Set all to the mode of the prior for T(0):
    INITIAL.VALS$Temperature<-PRIORS$T.0[1]*matrix(1,dim(BARCAST.INPUT$Inst.Data)[1]+1,dim(BARCAST.INPUT$Inst.Data)[2]);
  } else if ( Temp.Selector==1 ){
    #Method 2: Draw each T(k) from the prior for T(0):
    INITIAL.VALS$Temperature<-mvrnorm(dim(BARCAST.INPUT$Inst.Data)[1]+1,rep(PRIORS$T.0[1],dim(BARCAST.INPUT$Inst.Data)[2]),PRIORS$T.0[1]*diag(dim(BARCAST.INPUT$Inst.Data)[2]));
  }

  save(INITIAL.VALS, file=paste( BARCAST.datdir, "/INITIALVALSvNewMeth1.R", sep="") );
}
