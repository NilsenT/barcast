# This file calculates some error measures given
# - the reconstructed time series
# - the true time series
# over the calibration or verification interval
#
# The measures calculated are:
# CE (coefficient of efficiency)
# RE (reduction of error)
#
RE_CE <- function(Recon, Truth){
  value <- 1 - colSums( (Truth - Recon)**2)/colSums( (Recon - mean(Truth))**2)
  return(value)
}


