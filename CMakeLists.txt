cmake_minimum_required (VERSION 2.8)

# this needs to be set only for MacOS and is ignored on other systems
set(CMAKE_OSX_DEPLOYMENT_TARGET "10.4" CACHE STRING "Minimum OS X deployment version")

project (BARCAST)

IF(CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
    SET(CMAKE_INSTALL_PREFIX "~/BB_Barcast" CACHE PATH "setting install path non-global" FORCE)
ENDIF(CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)

# Force out-of-source build
if(${CMAKE_SOURCE_DIR} STREQUAL ${CMAKE_BINARY_DIR})
  message(FATAL_ERROR  "This project requires an out of source build. Remove the file 'CMakeCache.txt' found in this directory before continuing, create a separate build directory and run 'cmake [options] <srcs>' from there.")
endif()

# make sure that the default is a RELEASE
if (NOT CMAKE_BUILD_TYPE)
  set (CMAKE_BUILD_TYPE RELEASE CACHE STRING
      "Choose the type of build, options are: None Debug Release."
      FORCE)
endif (NOT CMAKE_BUILD_TYPE)

#execute_process( COMMAND module swap PrgEnv-gnu PrgEnv-pgi)
enable_language (Fortran)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${PROJECT_SOURCE_DIR}/Modules/")

# FFLAGS depend on the compiler
get_filename_component (Fortran_COMPILER_NAME ${CMAKE_Fortran_COMPILER} NAME)
# set( Fortran_COMPILER_NAME "pgfortran")

if (Fortran_COMPILER_NAME MATCHES "pgfortran.*")
  # pgi fortran (untested)
  message ("pgfortrn")
  set (CMAKE_Fortran_FLAGS_RELEASE "-O3 -fPIC")
  set (CMAKE_Fortran_FLAGS_DEBUG   "-O0 -g -fPIC")
elseif (Fortran_COMPILER_NAME MATCHES "gfortran.*")
  # gfortran
  set (CMAKE_Fortran_FLAGS_RELEASE "-funroll-all-loops -fno-f2c -O3 -fPIC")
  set (CMAKE_Fortran_FLAGS_DEBUG   "-fno-f2c -O0 -g -fPIC")
elseif (Fortran_COMPILER_NAME MATCHES "ifort.*")
  # ifort (untested)
  set (CMAKE_Fortran_FLAGS_RELEASE "-f77rtl -O3")
  set (CMAKE_Fortran_FLAGS_DEBUG   "-f77rtl -O0 -g")
elseif (Fortran_COMPILER_NAME MATCHES "g77")
  # g77
  set (CMAKE_Fortran_FLAGS_RELEASE "-funroll-all-loops -fno-f2c -O3 -m32")
  set (CMAKE_Fortran_FLAGS_DEBUG   "-fno-f2c -O0 -g -m32")
else (Fortran_COMPILER_NAME MATCHES "pgfortran.*")
  message ("CMAKE_Fortran_COMPILER full path: " ${CMAKE_Fortran_COMPILER})
  message ("Fortran compiler: " ${Fortran_COMPILER_NAME})
  message ("No optimized Fortran compiler flags are known, we just try -O2...")
  set (CMAKE_Fortran_FLAGS_RELEASE "-O2")
  set (CMAKE_Fortran_FLAGS_DEBUG   "-O0 -g")
endif (Fortran_COMPILER_NAME MATCHES "pgfortran.*")

# build the required libraries
set( LocalLibDir "${PROJECT_SOURCE_DIR}/src/local_libs")
include_directories ( "${LocalLibDir}")
add_subdirectory( "${LocalLibDir}")
add_library( skitlocal STATIC "${LocalLibDir}/blassm.f"
    "${LocalLibDir}/formats.f" "${LocalLibDir}/matvec.f" "${LocalLibDir}/spamown.f")
set_target_properties( skitlocal PROPERTIES OUTPUT_NAME skitlocal )

add_library( ziggurat STATIC "${LocalLibDir}/ziggurat.f90")
set_target_properties( ziggurat PROPERTIES OUTPUT_NAME ziggurat )

add_library( CovMatrices "${LocalLibDir}/CovMat_Functions.f90" "${LocalLibDir}/dkbesl.f")
set_target_properties( CovMatrices PROPERTIES OUTPUT_NAME CovMatrices)

# provide the correct dependencies, the ziggurat.mod is only created *during the
# compilation* of the libziggurat library - but it is needed as an input file
# for librandom.
add_custom_command (
  OUTPUT "ziggurat.mod"
  DEPENDS  ziggurat
)

add_library( random STATIC "${LocalLibDir}/random.f90" "ziggurat.mod")
add_dependencies( random  ziggurat)
set_target_properties( random PROPERTIES OUTPUT_NAME random)

add_library( sparseCovMat SHARED "${PROJECT_SOURCE_DIR}/src/T_Updater.f90" "${PROJECT_SOURCE_DIR}/src/Proxy_Updater_muldim.f90")
SET_TARGET_PROPERTIES( sparseCovMat PROPERTIES OUTPUT_NAME T_Updater)
add_dependencies( sparseCovMat random ziggurat skitlocal CovMatrices)

set( LOCALLIBS ziggurat random skitlocal CovMatrices)
target_link_libraries( sparseCovMat ${LOCALLIBS} )

set( OpenBLAS_HOME "~/lib/openblas/lib/cmake/openblas")
#set( CMAKE_PREFIX_PATH "${CMAKE_PREFIX_PATH}" "~/OpenBLAS/lib/cmake/openblas")

find_package (OpenBLAS)
if ( ${OpenBLAS_FOUND})
   include_directories(${OpenBLAS_INCLUDE_DIRS})
#   set( BLAS_FOUND ON)
#   set( LAPACK_FOUND ON)
#   set( BLAS_INCLUDE_DIRS "${OpenBLAS_INCLUDE_DIRS}")
   set( BLAS_LIBRARIES "${OpenBLAS_LIB}")
   target_link_libraries ( sparseCovMat ${OpenBLAS_LIB})
   message(STATUS ${BLAS_INCLUDE_DIRS})
endif ( ${OpenBLAS_FOUND} )

if ( NOT DEFINED BLAS_FOUND )
  find_package (BLAS REQUIRED)
  if ( ${BLAS_FOUND})
     include_directories(${BLAS_INCLUDE_DIRS})
     target_link_libraries ( sparseCovMat ${BLAS_LIBRARIES})
  endif ( ${BLAS_FOUND})
endif ( NOT DEFINED BLAS_FOUND )

if ( NOT DEFINED LAPACK_FOUND) 
  find_package (LAPACK REQUIRED)
  if (LAPACK_FOUND)
     include_directories(${LAPACK_INCLUDE_DIRS})
     target_link_libraries ( sparseCovMat ${LAPACK_LIBRARIES})
  endif (LAPACK_FOUND)
endif ( NOT DEFINED LAPACK_FOUND) 

#set( CMAKE_REQUIRED_LIBRARIES ${LAPACK_LIBRARIES})
#check_fortran_function_exists( "dpotrf" DPOTRF_WORKS)
#message( STATUS ${DPOTRF_WORKS} )

# What happens if we do not link against R?
# target_link_libraries ( sparseCovMat R)

set( BARCASTFILES BARCAST_SetInitialValues.R InitialparvalsvNewModel.R
                  BARCAST_PreSampler.R Common.R BARCAST_PreSwitchSampler.R spCovariancePatterns.R
                  PriorParsvNewModel.R BARCAST_Sampler_MC3.R Proxy_Time_Updater.R )

foreach( RFILES ${BARCASTFILES})
    install( FILES "${PROJECT_SOURCE_DIR}/bin/${RFILES}" DESTINATION bin)
endforeach( RFILES)
install( TARGETS sparseCovMat DESTINATION lib)
configure_file( "${PROJECT_SOURCE_DIR}/bin/BARCASTMainCode_Rmpi.R" "${CMAKE_INSTALL_PREFIX}/bin/BARCASTMainCode_Rmpi.R")

set( BARCASTHELPERS ConstructTestDatavNM.R Eval_TimeUncertain_Recon.R
                  ScoringRules.R Gather_Temp_Data.R)
foreach( RFILES ${BARCASTHELPERS})
    install( FILES "${PROJECT_SOURCE_DIR}/bin/helper_programs/${RFILES}" DESTINATION helper_programs)
endforeach( RFILES)
configure_file( "${PROJECT_SOURCE_DIR}/bin/helper_programs/MakeDistRecon.R" "${CMAKE_INSTALL_PREFIX}/helper_programs/MakeDistRecon.R")

set( TESTFILES BARCASTINPUTvNewMeth1.R PriorPars.RScript Certain+Uncertain_Data.RData)
foreach( RFILES ${TESTFILES})
  install( FILES "${PROJECT_SOURCE_DIR}/TestData/${RFILES}" DESTINATION TestData)
endforeach( RFILES)
install( FILES "${PROJECT_SOURCE_DIR}/bin/RunBarcastTest.sh" DESTINATION bin)

